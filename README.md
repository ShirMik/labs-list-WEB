## Список лабораторных работ по курсу Языков Интернет-Программирования
#### Широкий М. А., 2022 год
#### ИУ6-35Б, 25 вариант

В данном репозитории лежит README.md с описанием того, как и где можно получить ссылки на репозитории с лабораторными работами, сделанными мной. Также имеются пояснения к тем или иным лабораторным.

Будут даны как ссылки в self-hosted Gitea автора работ, так и зеркала их на Gitlab.
Зеркала на Gitlab сделаны с целью
- Доступа даже при упавшем сервере Gitea
- Как резервная копия

Зеркал на GitHub нет по идеологическим причинам.

Также из Gitea нельзя клонировать репы (автор этого текста не нашёл времени настроить нормальное клонирование через HTTPS).

### Лабораторные работы:
- Этот файл
	+ Ссылки: [Gitea](https://git.shymkl.ru/shirmik/labs-list-WEB "Оригинал") [Gitlab](https://gitlab.com/shirmik/labs-list-WEB "Зеркало Gitlab")
- ☑ 1-ая лабораторная работа - Основы HTML, CSS
	+ Ссылки: [Gitea](https://git.shymkl.ru/shirmik/1-and-2-labs-WEB "Оригинал") [Gitlab](https://gitlab.com/shirmik/1-and-2-labs-WEB "Зеркало Gitlab")
	+ Пояснение: 1-ая и 2-ая лабораторные работы были объединены
- ☑ 2-ая лабораторная работа: см. пункт выше
- ☑ 3-я лабораторная работа - Javascript
	+ Ссылки: [Gitea](https://git.shymkl.ru/shirmik/3-and-4-labs-WEB "Оригинал") [Gitlab](https://gitlab.com/shirmik/3-and-4-labs-WEB "Зеркало Gitlab")
	+ Пояснение: 3-я и 4-ая лабораторные работы были объединены
- ☑ 4-ая лабораторная работа: см. пункт выше
- ☑ 5-ая лабораторная работа - Основы Ruby и тестирования
	+ Ссылки: [Gitea](https://git.shymkl.ru/shirmik/5-lab-WEB "Оригинал") [Gitlab](https://gitlab.com/shirmik/5-lab-WEB "Зеркало Gitlab")
- ☑ 6-ая лабораторная работа - Enumerable/Enumerator
	+ Ссылки: [Gitea](https://git.shymkl.ru/shirmik/6-lab-WEB "Оригинал") [Gitlab](https://gitlab.com/shirmik/6-lab-WEB "Зеркало Gitlab")
- ☑ 7-ая лабораторная работа - Работа с файлами и классами в Ruby
	+ Ссылки: [Gitea](https://git.shymkl.ru/shirmik/7-lab-WEB "Оригинал") [Gitlab](https://gitlab.com/shirmik/7-lab-WEB "Зеркало Gitlab")
- ☑ 8-ая лабораторная работа - Ruby on Rails
	+ Ссылки: [Gitea](https://git.shymkl.ru/shirmik/8-and-9-labs-WEB "Оригинал") [Gitlab](https://gitlab.com/shirmik/8-and-9-labs-WEB "Зеркало Gitlab")
	+ Пояснение: так как в лабораторных 9-12 используется алгоритм из ЛР 8,
    код для них был опубликован как ветки 8-й лабы: laba9, laba10_1, laba11 и laba12 соответственно.
- ☑ 9-ая лабораторная работа - Turbo Rails
	+ Ссылки: [Gitea](https://git.shymkl.ru/shirmik/8-and-9-labs-WEB/src/branch/laba9 "Оригинал") [Gitlab](https://gitlab.com/shirmik/8-and-9-labs-WEB/-/tree/laba9 "Зеркало Gitlab")
- ☑ 10-ая лабораторная работа - XML, RSS, перенаправление, скачивание файлов, XSLT-преобразования
	+ Ссылки
		* Серверная часть - ветка от лабораторной работы 8: [Gitea](https://git.shymkl.ru/shirmik/8-and-9-labs-WEB/src/branch/laba10_1 "Оригинал") [Gitlab](https://gitlab.com/shirmik/8-and-9-labs-WEB/-/tree/laba10_1 "Зеркало Gitlab")
		* Клиентская часть - отдельный репозиторий: [Gitea](https://git.shymkl.ru/shirmik/10_2-lab-WEB "Оригинал") [Gitlab](https://gitlab.com/shirmik/10_2-lab-WEB "Зеркало Gitlab")
	+ Пояснение: данная лабораторная работа разделена на 2 отдельных веб-приложения.
	Первое - ответвление от ЛР 8, отдающее XML. Второе - читает, преобразует и скачивает
	XML из первого приложения.
- ☒ 11-ая лабораторная работа - Работа с моделями и базами данных
	+ Ссылки (ветка от лабораторной работы 8): [Gitea](https://git.shymkl.ru/shirmik/8-and-9-labs-WEB/src/branch/laba11 "Оригинал") [Gitlab](https://gitlab.com/shirmik/8-and-9-labs-WEB/-/tree/laba11 "Зеркало Gitlab")
- ☑ 12-ая лабораторная работа - Аутентификация и cookies
	+ Ссылки (ветка от лабораторной работы 8): [Gitea](https://git.shymkl.ru/shirmik/8-and-9-labs-WEB/src/branch/laba12 "Оригинал") [Gitlab](https://gitlab.com/shirmik/8-and-9-labs-WEB/-/tree/laba12 "Зеркало Gitlab")

#### Все лабораторные работы были зачтены

### Итоговая зачётная работа:
- Тема - сайт с отзывами на продукты
  + Ссылки: [Gitea](https://git.shymkl.ru/shirmik/final-lab-WEB "Оригинал") [Gitlab](https://gitlab.com/ShirMik/final-lab-WEB "Зеркало Gitlab")

